import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:tahfidzta_doaqu/page/ayat/record_model.dart';

class SambungAyatPage extends StatefulWidget {
  @override
  _SambungAyatPageState createState() => _SambungAyatPageState();
}

class _SambungAyatPageState extends State<SambungAyatPage> {
  @override
  void initState() {
    super.initState();
    print('[Page] Sambung Ayat');
  }

  Future<List<FileRecordModel>> _fetchFileRecordList() async {
    List<FileRecordModel> _fileRecordList = [];

    _fileRecordList
        .add(FileRecordModel(title: 'Al Fatihah', description: 'Pembuka'));
    _fileRecordList
        .add(FileRecordModel(title: 'Al Baqarah', description: 'Sapi Betina'));
    _fileRecordList.add(
        FileRecordModel(title: 'Ali Imran', description: 'Keluarga Imran'));
    _fileRecordList
        .add(FileRecordModel(title: 'An Nisa', description: 'Wanita'));
    _fileRecordList
        .add(FileRecordModel(title: 'Al Ma\'idah', description: 'Jamuan'));
    _fileRecordList
        .add(FileRecordModel(title: 'Al An\'am', description: 'Hewan Ternak'));
    _fileRecordList.add(FileRecordModel(
        title: 'Al-A\'raf', description: 'Tempat yang Tertinggi'));
    _fileRecordList.add(FileRecordModel(
        title: 'Al-Anfal', description: 'Harta Rampasan Perang'));
    _fileRecordList
        .add(FileRecordModel(title: 'At-Taubah', description: 'Pengampunan'));
    _fileRecordList
        .add(FileRecordModel(title: 'Yunus', description: 'Nabi Yunus'));
    _fileRecordList.add(FileRecordModel(title: 'Hud', description: 'Nabi Hud'));
    _fileRecordList
        .add(FileRecordModel(title: 'Yusuf', description: 'Nabi Yusuf'));
    _fileRecordList
        .add(FileRecordModel(title: 'Ar-Ra\'d', description: 'Guruh'));
    _fileRecordList
        .add(FileRecordModel(title: 'Ibrahim', description: 'Nabi Ibrahim'));
    _fileRecordList
        .add(FileRecordModel(title: 'Al-Hijr', description: 'Gunung Al Hijr'));

    return Future.delayed(Duration(seconds: 5), () {
      return _fileRecordList;
    });
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text(
                Base.appName,
                style: TextStyle(color: Colors.white),
              ),
              centerTitle: true,
              backgroundColor: TahDoPallete.green1,
              elevation: 0,
            ),
            body: FutureBuilder<List>(
              future: _fetchFileRecordList(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, position) {
                      return Column(
                        children: <Widget>[
                          _itemFileRecord(snapshot.data[position], position),
                          Divider(
                            thickness: 1,
                          ),
                        ],
                      );
                    },
                  );
                }
                return Center(
                  child: JumpingDotsProgressIndicator(
                    fontSize: FontSize.h2,
                    color: TahDoPallete.green1,
                  ),
                );
              },
            ),));
  }

  Widget _itemFileRecord(FileRecordModel fileRecordModel, var position) {
    String _titleData = fileRecordModel.title;
    String _descriptionData = fileRecordModel.description;

    return Container(
        margin: EdgeInsets.only(left: 8, right: 8),
        padding: EdgeInsets.all(8),
        child: Row(
          children: <Widget>[
            Image(
              image: AssetImage('assets/icon/icon_muslim.png'),
              height: 50,
              width: 50,
            ),
            Container(
              padding: EdgeInsets.only(left: 16),
              height: 40,
              alignment: Alignment.topLeft,
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '$_titleData',
                      style: TextStyle(
                          fontSize: FontSize.body1,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '$_descriptionData',
                      style: TextStyle(
                        fontSize: FontSize.body2,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            IconButton(
              icon: Icon(Icons.play_arrow_rounded),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              iconSize: 30,
              onPressed: () {
                print('[Icon Button] $_titleData | $_descriptionData');
              },
            )
          ],
        ));
  }
}
