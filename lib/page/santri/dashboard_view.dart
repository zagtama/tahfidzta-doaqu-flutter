import 'package:flutter/material.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';

class DashboardSantriPage extends StatefulWidget {
  @override
  _DashboardSantriPageState createState() => _DashboardSantriPageState();
}

class _DashboardSantriPageState extends State<DashboardSantriPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          Base.appName,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: TahDoPallete.green1,
        elevation: 0,
      ),
      body: ListView(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.all(16),
            decoration: (BoxDecoration(
                color: TahDoPallete.green2,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 4,
                      offset: Offset(0, 4))
                ])),
            child: Row(
              children: <Widget>[
                Container(
                  height: 100,
                  width: 100,
                  decoration: (BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(width: 5, color: Colors.white),
                      borderRadius: BorderRadius.all(Radius.circular(5)))),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 16),
                    child: Container(
                      height: 100,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'VANIA KEYSHA',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.body1,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'B12.001',
                            style: TextStyle(
                                color: Colors.white, fontSize: FontSize.body2),
                          ),
                          Text(
                            'Tembalang',
                            style: TextStyle(
                                color: Colors.white, fontSize: FontSize.body2),
                          ),
                          Spacer(),
                          Text(
                            'AKTIF',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.body1,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    )),
              ],
            ),
          ),
          Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
              decoration: (BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 1, color: Colors.black12),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        blurRadius: 4,
                        offset: Offset(0, 4))
                  ])),
              child: Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'DATA SANTRI',
                      style: TextStyle(
                          fontSize: FontSize.body1,
                          fontWeight: FontWeight.bold),
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Nomor Induk Santri', 'B20.001'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Nama Santri', 'Vania Keisya'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Tanggal Lahir', '12 Maret 2014'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Jenis Kelamin', 'Perempuan'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Nama Ayah', 'Winu Koentjoro'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Nama Ibu', 'Sari Puji Astuti'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Alamat', 'Wologito Raya No.27'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Cabang', 'Tembalang'),
                  ],
                ),
              )),
          Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
              decoration: (BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 1, color: Colors.black12),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        blurRadius: 4,
                        offset: Offset(0, 4))
                  ])),
              child: Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'RAPORT SANTRI',
                      style: TextStyle(
                          fontSize: FontSize.body1,
                          fontWeight: FontWeight.bold),
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Hapalan 1', 'Tuntas'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Hapalan 2', 'Belum Tuntas'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Hapalan 3', 'Tuntas'),
                    Divider(
                      thickness: 1,
                    ),
                    _itemDashboard('Hapalan 4', 'Tuntas'),
                  ],
                ),
              ))
        ],
      ),
    ));
  }

  Widget _itemDashboard(String title, String content) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 8),
          child: Text(
            title,
            style: TextStyle(
              fontSize: FontSize.caption,
            ),
          ),
        ),
        Text(
          content,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
