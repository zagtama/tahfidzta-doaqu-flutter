import 'package:flutter/material.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:tahfidzta_doaqu/page/news/content.dart';

class SoftLaunchingNews extends StatefulWidget {
  @override
  _SoftLaunchingNewsState createState() => _SoftLaunchingNewsState();
}

class _SoftLaunchingNewsState extends State<SoftLaunchingNews> {
  var _imgBanner = 'assets/images/bg3.jpg';

  void initState() {
    super.initState();
    print('Softlaunching was opened!');
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          Base.appName,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: TahDoPallete.green1,
        elevation: 0,
      ),
      body: Container(
        margin: (EdgeInsets.only(left: 16, right: 16)),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 16)),
              Container(
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          blurRadius: 4,
                          offset: Offset(0, 4)),
                    ],
                    image: DecorationImage(
                      image: AssetImage(_imgBanner),
                      fit: BoxFit.fill,
                    )),
                height: 120,
                width: double.infinity,
                child: Center(
                  child: Text(
                    'SOFT LAUNCHING\nTAHFIZTA DOAQU',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: FontSize.h6,
                      fontWeight: FontWeight.w200,
                      letterSpacing: 5,
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 16)),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  SoftLaunching().title,
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: FontSize.h6,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 4)),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  SoftLaunching().date,
                  style: TextStyle(
                      color: Colors.black54, fontSize: FontSize.body2),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 16)),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  SoftLaunching().content,
                  style: TextStyle(
                      color: Colors.black87, fontSize: FontSize.body1),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 32)),
            ],
          ),
        ),
      ),
    ));
  }
}
